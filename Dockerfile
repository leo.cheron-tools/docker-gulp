FROM alpine:latest

MAINTAINER Leo Cheron <leo@cheron.works>

# build dependencies
RUN apk --no-cache add python build-base

# nodejs
RUN apk --no-cache add nodejs nodejs-npm

# gulp
RUN npm i -g gulp

RUN mkdir -p /data
VOLUME /data
WORKDIR /data

CMD ["sh"]